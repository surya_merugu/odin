/**
  *****************************************************************************
  * Title                 :   CONVERSION KIT
  * Filename              :   re_can_int_callback.h
  * Origin Date           :   14/08/2020
  * Compiler              :   ICCARM Complier
  * Hardware              :   None
  * Target                :   STM32F446RE-NUCLEO
  * Notes                 :   None
  *****************************************************************************
  * @attention
  *
  * <h2><center>&copy;Copyright (C) RACEnergy, Inc - All Rights Reserved.
  * </center></h2>
  *
  * Unauthorized copying of this file, via any medium is strictly prohibited
  * Proprietary and confidential
  *
  * Written by Team RACEnergy, AUG 2020
  *****************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef _RE_CAN_INT_CALLBACK_H
#define _RE_CAN_INT_CALLBACK_H

/* Includes */
#include "stm32f4xx_hal.h"
#include "stm32f4xx_hal_can.h"
#include "main/main.h"
#include "string.h"
#include "re_std_def.h"
#include "dash_lcd/re_app_dash_lcd.h"
#include "re_can_init.h"
#include "stdio.h"
#include "main/main.h"
#include "dash_lcd/re_app_dash_lcd.h"

extern uint8_t onePackInfoBuffer[150];
extern uint8_t save_odo;
extern uint8_t key_status;
extern uint8_t animate;
extern bool onePackInfoBuffEmpty;
extern bool dockLatchClosed;
extern bool TransmitLatchStatus_Flag;
extern uint8_t Backup_BatVoltage;

#endif
/************************ (C) COPYRIGHT RACEnergy **********END OF FILE********/