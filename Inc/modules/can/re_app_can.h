/**
  *****************************************************************************
  * Title                 :   CONVERSION KIT
  * Filename              :   re_app_can.h
  * Origin Date           :   14/08/2020
  * Compiler              :   ICCARM Complier
  * Hardware              :   None
  * Target                :   STM32F446RE-NUCLEO
  * Notes                 :   None
  *****************************************************************************
  * @attention
  *
  * <h2><center>&copy;Copyright (C) RACEnergy, Inc - All Rights Reserved.
  * </center></h2>
  *
  * Unauthorized copying of this file, via any medium is strictly prohibited
  * Proprietary and confidential
  *
  * Written by Team RACEnergy, AUG 2020
  *****************************************************************************
  */

#ifndef __RE_APP_CAN_H
#define __RE_APP_CAN_H

#include "string.h"
#include "re_can_init.h"
//#include "re_can_int_callback.h"

typedef struct 
{
    uint32_t CanId;
    uint8_t PhyId[8];      
}BatInfo_t;

extern BatInfo_t BatInfo[4];
extern uint8_t battery_num;
RE_StatusTypeDef RE_Req_CapturedData(uint32_t batId);
RE_StatusTypeDef RE_Capture_Data(void);
RE_StatusTypeDef RE_Req_BatIds(void);
RE_StatusTypeDef RE_ReqPack_TurnOFF_DischargeFET(void);
RE_StatusTypeDef RE_ReqPack_TurnON_DischargeFET(void);
RE_StatusTypeDef RE_Latch_Open(void);
RE_StatusTypeDef RE_Rev_Light_Cmd(uint8_t rev_light_status);
RE_StatusTypeDef RE_Send_System_Start_Cmd(void);
RE_StatusTypeDef RE_Tx_Key_Ctrl_Cmd(uint8_t key_ctrl_status);

#endif
/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/