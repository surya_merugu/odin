/**
  *****************************************************************************
  * Title                 :   CONVERSION KIT
  * Filename              :   main.h
  * Origin Date           :   13/08/2020
  * Compiler              :   ICCARM Complier
  * Hardware              :   None
  * Target                :   STM32F446RE-NUCLEO
  * Notes                 :   None
  *****************************************************************************
  * @attention
  *
  * <h2><center>&copy;Copyright (C) RACEnergy, Inc - All Rights Reserved.
  * </center></h2>
  *
  * Unauthorized copying of this file, via any medium is strictly prohibited
  * Proprietary and confidential
  *
  * Written by Team RACEnergy, AUG 2020
  *****************************************************************************
  */
/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

/* Includes ------------------------------------------------------------------*/
#include "stm32f4xx_hal.h"
#include "re_sys_clk_config.h"
#include "re_std_def.h"
#include "re_can_init.h"
#include "re_uart_init.h"
#include "re_i2c_init.h"
#include "re_spi_init.h"
#include "re_lcd_init.h"
#include "dash_lcd/re_app_dash_lcd.h"
#include "dash_lcd/re_driver_dash_lcd.h"
#include "re_timer_init.h"
#include "re_rtc_init.h"
#include "idle_state/re_idle_state.h"
#include "re_ec20_init.h"
#include "uart/re_app_UART_Ring.h"
#include "re_gear_pos_init.h"

extern SD_HandleTypeDef hsd;
extern DMA_HandleTypeDef hdma_sdio_rx;
extern DMA_HandleTypeDef hdma_sdio_tx;
extern char allPackInfoBuffer[600];

extern void AppendLog(char *pFileName, char *pData);
extern void OverWriteLog(char *pFileName, char *pData);
extern uint32_t ReadLogFile(char *pFileName, uint8_t len);

#endif
/*********************** (C) COPYRIGHT RACEnergy **********END OF FILE********/