/**
  *****************************************************************************
  * Title                 :   CONVERSION KIT
  * Filename              :   re_app_uart_ring.h
  * Origin Date           :   14/08/2020
  * Compiler              :   ICCARM Complier
  * Hardware              :   None
  * Target                :   STM32F446RE-NUCLEO
  * Notes                 :   None
  *****************************************************************************
  * @attention
  *
  * <h2><center>&copy;Copyright (C) RACEnergy, Inc - All Rights Reserved.
  * </center></h2>
  *
  * Unauthorized copying of this file, via any medium is strictly prohibited
  * Proprietary and confidential
  *
  * Written by Team RACEnergy, AUG 2020
  *****************************************************************************
  */


#ifndef _RE_APP_UART_RING_H
#define _RE_APP_UART_RING_H

#include "re_uart_init.h"
#include "stm32f4xx_hal.h"
#include "string.h"
#include "stdbool.h"
#include "nrf52/re_app_nrf52.h"
#include "dash_lcd/re_app_dash_lcd.h"
#include "re_rtc_init.h"
#include "stdio.h"
#include "stdlib.h"

/* change the size of the buffer here */
#define UART1_BUFFER_SIZE 150
#define UART2_BUFFER_SIZE 16

extern bool ec20_bat_id_ack;

typedef struct
{
  unsigned char buffer_uart1[UART1_BUFFER_SIZE];
  volatile unsigned int head_uart1;
  volatile unsigned int tail_uart1;
}ring_buffer_uart1;

typedef struct
{
  unsigned char buffer_uart2[UART2_BUFFER_SIZE];
  volatile unsigned int head_uart2;
  volatile unsigned int tail_uart2;
}ring_buffer_uart2;

extern uint8_t ble_tx_buffer[16];
extern uint16_t LenOfble_tx_buffer;
extern volatile bool uart1RxCmplt;
extern volatile bool uart2RxCmplt;

/* Initialize the ring buffer */
void Ringbuf_Init(void);

/* Free buffer memory*/
extern void Ringbuf_UART1_Free(void);
extern void Ringbuf_UART2_Free(void);

/* once you hit 'enter' (\r\n), it copies the entire string to the buffer*/
extern void Get_UART1_String(char *buffer);
extern void Get_UART2_String(char *buffer);

/*After the predefined string is reached, it will copy the numberofchars after that into the buffertosave
 *USAGE---->>> if (Get_after("some string", 8, buffer)) { do something here}
 * */
//uint8_t Get_After(char *string, uint8_t numberofchars, char *buffertosave);

/* the ISR for the uart. put it in the IRQ handler */
extern void UART_ISR(UART_HandleTypeDef *huart);

//store uart string 
extern void Store_UART1_Char(unsigned char c, ring_buffer_uart1 *buffer);
extern void Store_UART2_Char(unsigned char c, ring_buffer_uart2 *buffer);

extern void UART1_HandleData(void);
extern void UART2_HandleData(void);

#endif
/*********************** (C) COPYRIGHT RACEnergy **********END OF FILE********/