/**
  *****************************************************************************
  * Title                 :   CONVERSION KIT
  * Filename              :   stm32f4xx_it.c
  * Origin Date           :   13/08/2020
  * Compiler              :   ICCARM Complier
  * Hardware              :   None
  * Target                :   STM32F446RE-NUCLEO
  * Notes                 :   None
  *****************************************************************************
  * @attention
  *
  * <h2><center>&copy;Copyright (C) RACEnergy, Inc - All Rights Reserved.
  * </center></h2>
  *
  * Unauthorized copying of this file, via any medium is strictly prohibited
  * Proprietary and confidential
  *
  * Written by Team RACEnergy, AUG 2020
  *****************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "stm32f4xx_it.h"

extern DMA_HandleTypeDef hdma_sdio_rx;
extern DMA_HandleTypeDef hdma_sdio_tx;
extern SD_HandleTypeDef hsd;

/**
  * @Brief This function handles Non maskable interrupt.
  */
void NMI_Handler(void)
{
    
}

/**
  * @Brief This function handles Hard fault interrupt.
  */
void HardFault_Handler(void)
{
    while (1)
    {

    }
}

/**
  * @Brief This function handles Memory management fault.
  */
void MemManage_Handler(void)
{
    while (1)
    {

    }
}

/**
  * @Brief This function handles Pre-fetch fault, memory access fault.
  */
void BusFault_Handler(void)
{
    while (1)
    {

    }
}

/**
  * @Brief This function handles Undefined instruction or illegal state.
  */
void UsageFault_Handler(void)
{
    while (1)
    {

    }
}

/**
  * @Brief This function handles System service call via SWI instruction.
  */
void SVC_Handler(void)
{
    
}

/**
  * @Brief This function handles Debug monitor.
  */
void DebugMon_Handler(void)
{
    
}

/**
  * @Brief This function handles Pendable request for system service.
  */
void PendSV_Handler(void)
{
    
}

/**
  * @Brief This function handles System tick timer.
  */
void SysTick_Handler(void)
{
    HAL_IncTick();
}

/******************************************************************************/
/* STM32F4xx Peripheral Interrupt Handlers                                    */
/* Add here the Interrupt Handlers for the used peripherals.                  */
/* For the available peripheral interrupt handler names,                      */
/* please refer to the startup file (startup_stm32f4xx.s).                    */
/******************************************************************************/

/**
  * @Brief This function handles SDIO global interrupt.
  */
void SDIO_IRQHandler(void)
{
    HAL_SD_IRQHandler(&hsd);
}

/**
  * @Brief This function handles DMA2 stream3 global interrupt.
  */
void DMA2_Stream3_IRQHandler(void)
{
    HAL_DMA_IRQHandler(&hdma_sdio_rx);
}

/**
  * @Brief This function handles DMA2 stream6 global interrupt.
  */
void DMA2_Stream6_IRQHandler(void)
{
    HAL_DMA_IRQHandler(&hdma_sdio_tx);
}

/**
  * @Brief This function handles CAN1 TX interrupts.
  */
void CAN1_TX_IRQHandler(void)
{
    HAL_CAN_IRQHandler(&hcan1_t);
}

/**
  * @Brief This function handles CAN1 RX0 interrupts.
  */
void CAN1_RX0_IRQHandler(void)
{
    HAL_CAN_IRQHandler(&hcan1_t);
}

/**
  * @Brief This function handles CAN1 RX1 interrupt.
  */
void CAN1_RX1_IRQHandler(void)
{
    HAL_CAN_IRQHandler(&hcan1_t);
}

/**
  * @Brief This function handles CAN1 SCE interrupt.
  */
void CAN1_SCE_IRQHandler(void)
{
    HAL_CAN_IRQHandler(&hcan1_t);
}

/**
  * @Brief This function handles CAN1 TX interrupts.
  */
void CAN2_TX_IRQHandler(void)
{
    HAL_CAN_IRQHandler(&hcan2_t);
}

/**
  * @Brief This function handles CAN1 RX0 interrupts.
  */
void CAN2_RX0_IRQHandler(void)
{
    HAL_CAN_IRQHandler(&hcan2_t);
}

/**
  * @Brief This function handles CAN1 RX1 interrupt.
  */
void CAN2_RX1_IRQHandler(void)
{
    HAL_CAN_IRQHandler(&hcan2_t);
}

/**
  * @Brief This function handles CAN1 SCE interrupt.
  */
void CAN2_SCE_IRQHandler(void)
{
    HAL_CAN_IRQHandler(&hcan2_t);
}

/**
  * @Brief This function handles USART2 global interrupt.
  */
void USART1_IRQHandler(void)
{
    UART_ISR(&huart1_t);
}

/**
  * @Brief This function handles USART2 global interrupt.
  */
void USART2_IRQHandler(void)
{
    UART_ISR(&huart2_t);
}

/**
  * @Brief This function handles USART2 global interrupt.
  */
void USART3_IRQHandler(void)
{
    HAL_UART_IRQHandler(&huart3_t);
}

/**
  * @Brief This function handles TIM2 global interrupt.
  */
void TIM2_IRQHandler(void)
{
    HAL_TIM_IRQHandler(&htim2_t);
}

/**
  * @Brief This function handles TIM3 global interrupt.
  */
void TIM3_IRQHandler(void)
{
    HAL_TIM_IRQHandler(&htim3_t);
}

/**
  * @Brief This function handles TIM5 global interrupt.
  */
void TIM5_IRQHandler(void)
{
    HAL_TIM_IRQHandler(&htim5_t);
}

/**
 * @Brief This function handles GPIO Pin0 interrupts
 */
void EXTI0_IRQHandler(void)
{
    HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_0);
}

/**
 * @Brief This function handles GPIO Pin1 interrupts
 */
void EXTI1_IRQHandler(void)
{
    HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_1);
}

/*********************** (C) COPYRIGHT RACEnergy **********END OF FILE********/