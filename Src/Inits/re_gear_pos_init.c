/**
  *****************************************************************************
  * Title                 :   CONVERSION KIT
  * Filename              :   re_gear_pos_init.c
  * Origin Date           :   03/09/2020
  * Compiler              :   ICCARM Complier
  * Hardware              :   None
  * Target                :   STM32F446RE-NUCLEO
  * Notes                 :   None
  *****************************************************************************
  * @attention
  *
  * <h2><center>&copy;Copyright (C) RACEnergy, Inc - All Rights Reserved.
  * </center></h2>
  *
  * Unauthorized copying of this file, via any medium is strictly prohibited
  * Proprietary and confidential
  *
  * Written by Team RACEnergy, SEP 2020
  *****************************************************************************
  */

/* Includes */
#include "re_gear_pos_init.h"

uint8_t gear_sensor_status_1, gear_sensor_status_2;
GPIO_InitTypeDef gear_sense_1, gear_sense_2;

RE_StatusTypeDef RE_Gear_Pos_Init (void)
{   
    __HAL_RCC_GPIOC_CLK_ENABLE();
    /**
    * PC0  : gear_sense_1
    * PC1  : gear_sense_2
    */
    gear_sense_1.Pin     = GPIO_PIN_0; 
    gear_sense_1.Mode    = GPIO_MODE_INPUT;
    gear_sense_1.Pull    = GPIO_NOPULL;
    gear_sense_1.Speed   = GPIO_SPEED_HIGH;
    HAL_GPIO_Init(GPIOC, &gear_sense_1);
    
    gear_sense_2.Pin     = GPIO_PIN_1; 
    gear_sense_2.Mode    = GPIO_MODE_INPUT;
    gear_sense_2.Pull    = GPIO_NOPULL;
    gear_sense_2.Speed   = GPIO_SPEED_HIGH;
    HAL_GPIO_Init(GPIOC, &gear_sense_2);    
    return RE_OK;
}

RE_StatusTypeDef RE_Load_Gear_Pos (void)
{
    bool state_change = false;
    uint8_t rev_mode = false;
    if (gear_sensor_status_1 == HAL_GPIO_ReadPin(GPIOC, gear_sense_1.Pin))
    {
        state_change |= false;
    }
    else
    {
        state_change |= true;
    }
    if (gear_sensor_status_2 == HAL_GPIO_ReadPin(GPIOC, gear_sense_2.Pin))
    {
        state_change |= false;
    }
    else
    {
        state_change |= true;
    }
    gear_sensor_status_1 = HAL_GPIO_ReadPin(GPIOC, gear_sense_1.Pin);
    gear_sensor_status_2 = HAL_GPIO_ReadPin(GPIOC, gear_sense_2.Pin);
    if (gear_sensor_status_2) /** Neutral */
    {
        LcdDisplay.gear_direction = 1;
        rev_mode = 0;
    }
    else if (gear_sensor_status_1) /** Reverse */
    {
        LcdDisplay.gear_direction = 2;
        rev_mode = 1;
    }
    else /** Drive */
    {
        LcdDisplay.gear_direction = 3;
        rev_mode = 0;
    }
    if (state_change)
    {
        RE_Rev_Light_Cmd(rev_mode);
        state_change = false;
    }
    
    return RE_OK;
}
/************************ (C) COPYRIGHT RACEnergy **********END OF FILE********/