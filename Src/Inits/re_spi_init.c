/**
  *****************************************************************************
  * Title                 :   CONVERSION KIT
  * Filename              :   re_spi_init.c
  * Origin Date           :   14/08/2020
  * Compiler              :   ICCARM Complier
  * Hardware              :   None
  * Target                :   STM32F446RE-NUCLEO
  * Notes                 :   None
  *****************************************************************************
  * @attention
  *
  * <h2><center>&copy;Copyright (C) RACEnergy, Inc - All Rights Reserved.
  * </center></h2>
  *
  * Unauthorized copying of this file, via any medium is strictly prohibited
  * Proprietary and confidential
  *
  * Written by Team RACEnergy, AUG 2020
  *****************************************************************************
  */

/* Includes */
#include "re_spi_init.h"

SPI_HandleTypeDef hspi1_t;

/**
  * @Brief RE_SPI_GpioInit
  * This function initialises the GPIO's used by SPI peripheral
  * @Param hspi: SPI handle pointer
  * @Retval Exit status
  */
static RE_StatusTypeDef RE_SPI_GpioInit(SPI_HandleTypeDef *hspi)
{
    GPIO_InitTypeDef SPI_GPIO = {0};
    if(hspi->Instance == SPI1)
    {
        /* Peripheral clock enable */
        __HAL_RCC_SPI1_CLK_ENABLE();
        __HAL_RCC_GPIOA_CLK_ENABLE();
        __HAL_RCC_GPIOC_CLK_ENABLE();
        /** SPI1 GPIO Configuration
        * PA5     ------> SPI1_SCK
        * PA6     ------> SPI1_MISO
        * PA7     ------> SPI1_MOSI  
        * PC4    ------> CS/SS(active low)
        */
        SPI_GPIO.Pin              = GPIO_PIN_5 | GPIO_PIN_6 | GPIO_PIN_7;
        SPI_GPIO.Mode             = GPIO_MODE_OUTPUT_OD;
        SPI_GPIO.Pull             = GPIO_PULLUP;
        SPI_GPIO.Speed            = GPIO_SPEED_FREQ_VERY_HIGH;
        SPI_GPIO.Alternate        = GPIO_AF5_SPI1;;
        HAL_GPIO_Init(GPIOA, &SPI_GPIO);

        SPI_GPIO.Pin              = GPIO_PIN_4;
        SPI_GPIO.Mode             = GPIO_MODE_OUTPUT_PP;
        SPI_GPIO.Pull             = GPIO_PULLDOWN;
        SPI_GPIO.Speed            = GPIO_SPEED_FREQ_LOW;        
        HAL_GPIO_Init(GPIOC, &SPI_GPIO);
    }
    return RE_OK;
}

/**
  * @Brief RE_SPI_Init
  * This function configures the SPI peripheral
  * @Param None
  * @Retval Exit status
  */
RE_StatusTypeDef RE_SPI_Init(void)
{
      hspi1_t.Instance                   = SPI1;
      hspi1_t.Init.Mode                  = SPI_MODE_MASTER;
      hspi1_t.Init.Direction             = SPI_DIRECTION_2LINES;
      hspi1_t.Init.DataSize              = SPI_DATASIZE_16BIT;
      hspi1_t.Init.CLKPolarity           = SPI_POLARITY_LOW;
      hspi1_t.Init.CLKPhase              = SPI_PHASE_1EDGE;
      hspi1_t.Init.NSS                   = SPI_NSS_SOFT;
      hspi1_t.Init.BaudRatePrescaler     = SPI_BAUDRATEPRESCALER_16;
      hspi1_t.Init.FirstBit              = SPI_FIRSTBIT_MSB;
      hspi1_t.Init.TIMode                = SPI_TIMODE_DISABLE;
      hspi1_t.Init.CRCCalculation        = SPI_CRCCALCULATION_DISABLE;
      hspi1_t.Init.CRCPolynomial         = 10;
      if(RE_SPI_GpioInit(&hspi1_t) == RE_OK)
      {
          if (HAL_SPI_Init(&hspi1_t) != HAL_OK)
          {
            RE_Error_Handler(__FILE__, __LINE__);
          }
      }
    return RE_OK;
}
/************************ (C) COPYRIGHT RACEnergy **********END OF FILE********/