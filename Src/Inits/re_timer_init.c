/**
  *****************************************************************************
  * Title                 :   CONVERSION KIT
  * Filename              :   re_timer_init.c
  * Origin Date           :   14/08/2020
  * Compiler              :   ICCARM Complier
  * Hardware              :   None
  * Target                :   STM32F446RE-NUCLEO
  * Notes                 :   None
  *****************************************************************************
  * @attention
  *
  * <h2><center>&copy;Copyright (C) RACEnergy, Inc - All Rights Reserved.
  * </center></h2>
  *
  * Unauthorized copying of this file, via any medium is strictly prohibited
  * Proprietary and confidential
  *
  * Written by Team RACEnergy, AUG 2020
  *****************************************************************************
  */

/* Includes */
#include "re_timer_init.h"

TIM_HandleTypeDef htim3_t, htim2_t, htim5_t;

static RE_StatusTypeDef RE_Timer_ClkInit (TIM_HandleTypeDef* htim_base);

/**
  * @Brief RE_Timer_ClkInit
  * This function configures clock and set interrupt priority for the timer
  * @Param htim_base: TIM_HandleTypeDef
  * @Retval Exit status
  */
static RE_StatusTypeDef RE_Timer_ClkInit (TIM_HandleTypeDef* htim_base)
{
    if(htim_base -> Instance == TIM2)
    {
        __HAL_RCC_TIM2_CLK_ENABLE();
        HAL_NVIC_SetPriority(TIM2_IRQn, 0, 1);
        HAL_NVIC_EnableIRQ(TIM2_IRQn);
    }
    else if(htim_base -> Instance == TIM3)
    {
        __HAL_RCC_TIM3_CLK_ENABLE();
        HAL_NVIC_SetPriority(TIM3_IRQn, 0, 0);
        HAL_NVIC_EnableIRQ(TIM3_IRQn);
    }
    else if(htim_base -> Instance == TIM5)
    {
        __HAL_RCC_TIM5_CLK_ENABLE();
        HAL_NVIC_SetPriority(TIM5_IRQn, 0, 0);
        HAL_NVIC_EnableIRQ(TIM5_IRQn);
    }
    else
    {
        __NOP();
    }
    return RE_OK;
}

/**
  * @Brief RE_TIMER2_Init
  * This function configures Timer2 peripheral
  * @Param None
  * @Retval Exit status
  */
RE_StatusTypeDef RE_TIMER2_Init (void)
{
    TIM_ClockConfigTypeDef sClockSourceConfig;
    TIM_MasterConfigTypeDef sMasterConfig;
    htim2_t.Instance                           = TIM2;
    if(RE_Timer_ClkInit(&htim2_t) != RE_OK)
    {
        RE_Error_Handler(__FILE__, __LINE__);
    }
    htim2_t.Init.Prescaler                     = 49999;
    htim2_t.Init.CounterMode                   = TIM_COUNTERMODE_UP;
    htim2_t.Init.Period                        = 89;
    htim2_t.Init.ClockDivision                 = TIM_CLOCKDIVISION_DIV1;
    htim2_t.Init.AutoReloadPreload             = TIM_AUTORELOAD_PRELOAD_DISABLE;
    if (HAL_TIM_Base_Init(&htim2_t) != HAL_OK)
    {
        RE_Error_Handler(__FILE__, __LINE__);
    }
    sClockSourceConfig.ClockSource             = TIM_CLOCKSOURCE_INTERNAL;
    if(HAL_TIM_ConfigClockSource(&htim2_t, &sClockSourceConfig) != HAL_OK)
    {
        RE_Error_Handler(__FILE__, __LINE__);
    }
    sMasterConfig.MasterOutputTrigger          = TIM_TRGO_RESET;
    sMasterConfig.MasterSlaveMode              = TIM_MASTERSLAVEMODE_DISABLE;
    if (HAL_TIMEx_MasterConfigSynchronization(&htim2_t, &sMasterConfig) != HAL_OK)
    {
        RE_Error_Handler(__FILE__, __LINE__);
    }
    return RE_OK;
}

/**
  * @Brief RE_TIMER3_Init
  * This function configures Timer3 peripheral
  * @Param None
  * @Retval Exit status
  */
RE_StatusTypeDef RE_TIMER3_Init (void)
{
    TIM_ClockConfigTypeDef sClockSourceConfig;
    TIM_MasterConfigTypeDef sMasterConfig;
    htim3_t.Instance                           = TIM3;
    if(RE_Timer_ClkInit(&htim3_t) != RE_OK)
    {
        RE_Error_Handler(__FILE__, __LINE__);
    }
    htim3_t.Init.Prescaler                     = 47999;
    htim3_t.Init.CounterMode                   = TIM_COUNTERMODE_UP;
    htim3_t.Init.Period                        = 3749;
    htim3_t.Init.ClockDivision                 = TIM_CLOCKDIVISION_DIV1;
    htim3_t.Init.AutoReloadPreload             = TIM_AUTORELOAD_PRELOAD_DISABLE;
    if(HAL_TIM_Base_Init(&htim3_t) != HAL_OK)
    {
        RE_Error_Handler(__FILE__, __LINE__);
    }
    sClockSourceConfig.ClockSource             = TIM_CLOCKSOURCE_INTERNAL;
    if(HAL_TIM_ConfigClockSource(&htim3_t, &sClockSourceConfig) != HAL_OK)
    {
        RE_Error_Handler(__FILE__, __LINE__);
    }
    sMasterConfig.MasterOutputTrigger          = TIM_TRGO_RESET;
    sMasterConfig.MasterSlaveMode              = TIM_MASTERSLAVEMODE_DISABLE;
    if (HAL_TIMEx_MasterConfigSynchronization(&htim3_t, &sMasterConfig) != HAL_OK)
    {
        RE_Error_Handler(__FILE__, __LINE__);
    }
    return RE_OK;
}

/**
  * @Brief RE_TIMER5_Init
  * This function configures Timer5 peripheral
  * @Param None
  * @Retval Exit status
  */
RE_StatusTypeDef RE_TIMER5_Init (void)
{
    TIM_ClockConfigTypeDef sClockSourceConfig;
    TIM_MasterConfigTypeDef sMasterConfig;
    htim5_t.Instance                           = TIM5;
    if(RE_Timer_ClkInit(&htim5_t) != RE_OK)
    {
        RE_Error_Handler(__FILE__, __LINE__);
    }
    htim5_t.Init.Prescaler                     = 49999;
    htim5_t.Init.CounterMode                   = TIM_COUNTERMODE_UP;
    htim5_t.Init.Period                        = 899;
    htim5_t.Init.ClockDivision                 = TIM_CLOCKDIVISION_DIV1;
    htim5_t.Init.AutoReloadPreload             = TIM_AUTORELOAD_PRELOAD_DISABLE;
    if(HAL_TIM_Base_Init(&htim5_t) != HAL_OK)
    {
        RE_Error_Handler(__FILE__, __LINE__);
    }
    sClockSourceConfig.ClockSource             = TIM_CLOCKSOURCE_INTERNAL;
    if(HAL_TIM_ConfigClockSource(&htim5_t, &sClockSourceConfig) != HAL_OK)
    {
        RE_Error_Handler(__FILE__, __LINE__);
    }
    sMasterConfig.MasterOutputTrigger          = TIM_TRGO_RESET;
    sMasterConfig.MasterSlaveMode              = TIM_MASTERSLAVEMODE_DISABLE;
    if (HAL_TIMEx_MasterConfigSynchronization(&htim5_t, &sMasterConfig) != HAL_OK)
    {
        RE_Error_Handler(__FILE__, __LINE__);
    }
    return RE_OK;
}

/**
  * @Brief RE_Timers_Start
  * This function starts Timer2 and Timer3 in interuupts mode
  * @Param None
  * @Retval Exit status
  */
RE_StatusTypeDef RE_Timers_Start(void)
{
     RE_Capture_Data();
    /** Start 50ms timer */
    if(HAL_TIM_Base_Start_IT(&htim2_t) != HAL_OK)
    {
        RE_Error_Handler(__FILE__, __LINE__);
    }       
    /** Start 1000ms timer */
    if(HAL_TIM_Base_Start_IT(&htim3_t) != HAL_OK)
    {
        RE_Error_Handler(__FILE__, __LINE__);
    }
    return RE_OK;
}
/************************ (C) COPYRIGHT RACEnergy **********END OF FILE********/