/**
  *****************************************************************************
  * Title                 :   CONVERSION KIT
  * Filename              :   re_idle_state.c
  * Origin Date           :   17/08/2020
  * Compiler              :   ICCARM Complier
  * Hardware              :   None
  * Target                :   STM32F446RE-NUCLEO
  * Notes                 :   None
  *****************************************************************************
  * @attention
  *
  * <h2><center>&copy;Copyright (C) RACEnergy, Inc - All Rights Reserved.
  * </center></h2>
  *
  * Unauthorized copying of this file, via any medium is strictly prohibited
  * Proprietary and confidential
  *
  * Written by Team RACEnergy, AUG 2020
  *****************************************************************************
  */
/* Includes*/
#include "idle_state/re_idle_state.h"

uint8_t bat_num = 0;
uint8_t retryVerifyCnt;
bool ec20_bat_id_ack = false;

/**
 * @Brief RE_Idle_State_Handler
 * this function handles tasks in CPU idle mode
 * @Param None
 * @Retval Exit status
 */
RE_StatusTypeDef RE_Idle_State_Handler(void)
{
    if(animate == 1)
    {
        RE_LCD_ON();
        animate = 0;
    }    
    if (LcdDisplay.lock_status == 0) 
    {
        if(pack_id_req_pending)
        {    
            RE_Req_BatIds();
            packsIdVerified = true;  /* DEBUG ONLY */
            RE_Timers_Start(); 
            pack_id_req_pending = false;
            update_bat_id = true;
        }
    }
    else
    {
        pack_id_req_pending = true;
    }    
    if(packsIdVerified == true)
    {
        if(t50ms_Flag == true)
        {
            if((ec20_bat_id_ack == true) && (onePackInfoBuffEmpty != true))
            {
                strcat((char *)onePackInfoBuffer, "\n");
                uint16_t LenOfOnePackInfoBuffer = strlen((char *)onePackInfoBuffer);
                HAL_UART_Transmit(&huart1_t, onePackInfoBuffer, LenOfOnePackInfoBuffer, 200);
                memset(onePackInfoBuffer, 0, sizeof(onePackInfoBuffer));
                onePackInfoBuffEmpty = true;              
            }
            sprintf((char *)onePackInfoBuffer, "%s", "12,");                
            if(bat_num <= 3)
            {
                RE_Req_CapturedData(BatInfo[bat_num].CanId);
            }
            bat_num++;
            t50ms_Flag = false;
        }
        if(t2000ms_Flag == true)
        {
            if (ec20_lcd_data_rx == false )
            {
                ec20_lcd_data_rx_fail_cnt++;
                if(ec20_lcd_data_rx_fail_cnt >= 5)
                {
                    LcdDisplay.bat_volt         = 0;
                    LcdDisplay.bat_soc          = 0;
                    LcdDisplay.bat_temp         = 0;  
                    LcdDisplay.efficiency       = 0;
                }
            }
            ec20_lcd_data_rx = false;
            if (ec20_bat_id_ack == true)
            {
                uint8_t kit_info_buff[25];
                uint16_t kit_info_buff_len = strlen((char *)kit_info_buff);
                sprintf((char *)kit_info_buff, "%s,%d,%d,%d,%d,%d,%d\n", "17", key_status, LcdDisplay.lock_status,\
                            LcdDisplay.motor_fault_graph, LcdDisplay.speed, LcdDisplay.odo, Backup_BatVoltage);
                HAL_UART_Transmit(&huart1_t, kit_info_buff, kit_info_buff_len, 500);                  
                sprintf((char *)onePackInfoBuffer,"12,END\n");
                HAL_UART_Transmit(&huart1_t, onePackInfoBuffer, 7, 200);
            }
            char FileName[8];
            strcpy(FileName, "bat.txt");
            AppendLog(FileName, allPackInfoBuffer);
            bat_num = 0;                            /* Reset battery count */
            RE_Capture_Data();                      /* Send Capture SoC Cmd */
            RE_GetTimeStamp();
            sprintf(allPackInfoBuffer,"\n%d/%d/%d,%d:%d:%d",sDate_t.Date, sDate_t.Month, sDate_t.Year, sTime_t.Hours,\
                     sTime_t.Minutes, sTime_t.Seconds);
            if(HAL_TIM_Base_Start_IT(&htim2_t) != HAL_OK)  /* Start 50ms timer */
            {
                RE_Error_Handler(__FILE__, __LINE__);
            }
           t2000ms_Flag = false;
        }
    }
    if(t500ms_Flag == true)
    {
        if(update_bat_id)
        {
            update_bat_id = false;                
            RE_Tx_Bat_ID_EC20();
        }
        if(key_status == 1)
        {
            RE_Load_Gear_Pos();
            RE_RefreshScreen();
        }
        else
        {
            RE_lcd_power_off();
        }
        if((EnableEC20 == true) && (ec20_bat_id_ack == true))
        {
            EC20_Req_Timestamp_Counter++;
            if(EC20_Req_Timestamp_Counter % 20 == 0)
            {
                if(WaitForEC20Timestamp == true)
                {
                    RE_ReqEC20_Timestamp();
                }
                else if(WaitForEC20Timestamp == false)
                {
                    EnableEC20 = false;
                    EC20_Req_Timestamp_Counter = 0;
                }
            }
        }       
        t500ms_Flag = false;
    }
    if(Req_SwapData_From_EC20_Flag == true)
    {
        Req_SwapData_From_EC20_Flag = false;
        RE_Req_SwapData();
    }
    if(uart1RxCmplt == true)
    {
        UART1_HandleData();
        uart1RxCmplt = false;
    } 
    if(uart2RxCmplt == true)
    {
        UART2_HandleData();
        uart2RxCmplt = false;
    }
    if(TransmitLatchStatus_Flag == true)
    {
        sprintf((char *)ble_tx_buffer, "%s%d\n", "12", LcdDisplay.lock_status);
        LenOfble_tx_buffer = strlen((char *)ble_tx_buffer);
        HAL_UART_Transmit(&huart2_t, ble_tx_buffer, LenOfble_tx_buffer, 100);        
    }
    if(LcdDisplay.lock_status && SwapModeFlag && VerifyPacks)
    {
        uint8_t Id_Buffer[100];
        uint8_t MisPlacedBattery = 0;
        if(strcmp((char *)InitialSwapInfo[0].pid, (char *)BatInfo[0].PhyId) && strcmp((char *)InitialSwapInfo[0].pid, (char *)BatInfo[1].PhyId) &&\
            strcmp((char *)InitialSwapInfo[0].pid, (char *)BatInfo[2].PhyId) && strcmp((char *)InitialSwapInfo[0].pid, (char *)BatInfo[3].PhyId))
        {
            memset(ble_tx_buffer, 0, sizeof(ble_tx_buffer));
            sprintf((char *)ble_tx_buffer, "%s%d%s\n", "14", 0, InitialSwapInfo[0].pid);
            LenOfble_tx_buffer = strlen((char *)ble_tx_buffer);
            HAL_UART_Transmit(&huart2_t, ble_tx_buffer, LenOfble_tx_buffer, 100);        
            MisPlacedBattery++;
        }
        if(strcmp((char *)InitialSwapInfo[1].pid, (char *)BatInfo[0].PhyId) && strcmp((char *)InitialSwapInfo[1].pid, (char *)BatInfo[1].PhyId) &&\
            strcmp((char *)InitialSwapInfo[1].pid, (char *)BatInfo[2].PhyId) && strcmp((char *)InitialSwapInfo[1].pid, (char *)BatInfo[3].PhyId))
        {
            memset(ble_tx_buffer, 0, sizeof(ble_tx_buffer));
            sprintf((char *)ble_tx_buffer, "%s%d%s\n", "14", 0, InitialSwapInfo[1].pid);
            LenOfble_tx_buffer = strlen((char *)ble_tx_buffer);
            HAL_UART_Transmit(&huart2_t, ble_tx_buffer, LenOfble_tx_buffer, 100);         
            MisPlacedBattery++;
        }
        if(strcmp((char *)InitialSwapInfo[2].pid, (char *)BatInfo[0].PhyId) && strcmp((char *)InitialSwapInfo[2].pid, (char *)BatInfo[1].PhyId) &&\
            strcmp((char *)InitialSwapInfo[2].pid, (char *)BatInfo[2].PhyId) && strcmp((char *)InitialSwapInfo[2].pid, (char *)BatInfo[3].PhyId))
        {
            memset(ble_tx_buffer, 0, sizeof(ble_tx_buffer));
            sprintf((char *)ble_tx_buffer, "%s%d%s\n", "14", 0, InitialSwapInfo[2].pid);
            LenOfble_tx_buffer = strlen((char *)ble_tx_buffer);
            HAL_UART_Transmit(&huart2_t, ble_tx_buffer, LenOfble_tx_buffer, 100);         
            MisPlacedBattery++;
        }
        if(strcmp((char *)InitialSwapInfo[3].pid, (char *)BatInfo[0].PhyId) && strcmp((char *)InitialSwapInfo[3].pid, (char *)BatInfo[1].PhyId) &&\
            strcmp((char *)InitialSwapInfo[3].pid, (char *)BatInfo[2].PhyId) && strcmp((char *)InitialSwapInfo[3].pid, (char *)BatInfo[3].PhyId))
        {
            memset(ble_tx_buffer, 0, sizeof(ble_tx_buffer));
            sprintf((char *)ble_tx_buffer, "%s%d%s\n", "14", 0, InitialSwapInfo[3].pid);
            LenOfble_tx_buffer = strlen((char *)ble_tx_buffer);
            HAL_UART_Transmit(&huart2_t, ble_tx_buffer, LenOfble_tx_buffer, 100);      
            MisPlacedBattery++;
        }
        if(MisPlacedBattery == 0)
        {
            packsIdVerified = true;
            SwapModeFlag = false;
            memset(ble_tx_buffer, 0, sizeof(ble_tx_buffer));
            sprintf((char *)ble_tx_buffer, "%s%d\n", "14", 1);
            LenOfble_tx_buffer = strlen((char *)ble_tx_buffer);
            HAL_Delay(1000);
            HAL_UART_Transmit(&huart2_t, ble_tx_buffer, LenOfble_tx_buffer, 100);
        }
        VerifyPacks = false;
        sprintf((char *)Id_Buffer, "%s,%d|n%s,%d|n%s,%d|n%s,%d|n", BatInfo[0].PhyId, BatInfo[0].CanId,\
          BatInfo[1].PhyId, BatInfo[1].CanId, BatInfo[2].PhyId, BatInfo[2].CanId, BatInfo[3].PhyId, BatInfo[3].CanId);
        OverWriteLog("BatID.TXT", (char *)Id_Buffer);
    }  
    return RE_OK;
}
/*********************** (C) COPYRIGHT RACEnergy **********END OF FILE********/