/**
  *****************************************************************************
  * Title                 :   CONVERSION KIT
  * Filename              :   re_app_nvs.c
  * Origin Date           :   06/10/2020
  * Compiler              :   ICCARM Complier
  * Hardware              :   None
  * Target                :   STM32F4-DISCOVERY
  * Notes                 :   None
  *****************************************************************************
  * @attention
  *
  * <h2><center>&copy;Copyright (C) RACEnergy, Inc - All Rights Reserved.
  * </center></h2>
  *
  * Unauthorized copying of this file, via any medium is strictly prohibited
  * Proprietary and confidential
  *
  * Written by Team RACEnergy, OCT 2020
  *****************************************************************************
  */

/* Includes */
#include "nvs/re_app_nvs.h"

#define EEPROM_ADDRESS 0xA0
uint8_t SysConfig_Buffer[sizeof(InitialSwapInfo)];

RE_StatusTypeDef RE_Write_Struct_To_NVS(uint8_t bat_num)
{
    HAL_Delay(50);
    uint8_t MemAddr = 0;
    uint8_t * AddressofStruct = (uint8_t*)(&InitialSwapInfo[bat_num]);
    switch(bat_num)
    {
        case 0:
            MemAddr = 0;
            break;
        case 1:
            MemAddr = 11;
            break;
        case 2:
            MemAddr = 32;
            break;
        case 3:
            MemAddr = 42;
            break;
    }
    if(HAL_I2C_Mem_Write(&hi2c1_t, EEPROM_ADDRESS,  MemAddr, 0xFFFF, AddressofStruct, sizeof(InitialSwapInfo), 100) != HAL_OK)
    {
        RE_Error_Handler(__FILE__, __LINE__);
    }
    return RE_OK;
}

RE_StatusTypeDef RE_Read_Struct_From_NVS(uint16_t MemAddr, uint8_t bat_num)
{
    HAL_Delay(50);
    if(HAL_I2C_Mem_Read(&hi2c1_t, EEPROM_ADDRESS, MemAddr, 0xFFFF, SysConfig_Buffer, sizeof(InitialSwapInfo), 100) != HAL_OK)
    {
        RE_Error_Handler(__FILE__, __LINE__);
    }
    for(uint8_t i = 0; i <= 7; i++)
    {
        InitialSwapInfo[bat_num].pid[i] = SysConfig_Buffer[i];
    }
    InitialSwapInfo[bat_num].Energy = ((SysConfig_Buffer[9] << 8) | SysConfig_Buffer[8]);
    InitialSwapInfo[bat_num].Alert  = SysConfig_Buffer[10];
    return RE_OK;
}

RE_StatusTypeDef RE_WriteBytetoNVS(uint16_t MemAddr, uint8_t Data)
{
    if(HAL_I2C_Mem_Write(&hi2c1_t, EEPROM_ADDRESS,  MemAddr, 0xFFFF, &Data, 1, 100) != HAL_OK)
    {
        RE_Error_Handler(__FILE__, __LINE__);
    }
    return RE_OK;
}

RE_StatusTypeDef RE_Write_DwordToNVS(uint16_t MemAddr, uint8_t * Data)
{
    if(HAL_I2C_Mem_Write(&hi2c1_t, EEPROM_ADDRESS,  0, 0xFFFF, Data, 4, 100) != HAL_OK)
    {
        RE_Error_Handler(__FILE__, __LINE__);
    }
    return RE_OK;   
}
/************************ (C) COPYRIGHT RACEnergy **********END OF FILE********/